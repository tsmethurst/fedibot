/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package test

import "codeberg.org/tsmethurst/fedibot/pkg/model"

func GetSomePosts() []*model.Post {
	return []*model.Post{
		{
			ContentWarning: "this is the first post",
			Text:           "this is the text body of the first post",
			Language:       "en",
			Visibility:     "public",
		},
		{
			ContentWarning: "this is the second post",
			Text:           "this is the text body of the second post",
			Language:       "en",
			Visibility:     "public",
		},
		{
			ContentWarning: "this is the third post",
			Text:           "this is the text body of the third post",
			Language:       "en",
			Visibility:     "public",
		},
		{
			ContentWarning: "this is the fourth post",
			Text:           "this is the text body of the fourth post",
			Language:       "en",
			Visibility:     "public",
		},
	}
}

func GetAnotherPost() *model.Post {
	return &model.Post{
		ContentWarning: "this is the fifth post",
		Text:           "this is the text body of the fifth post",
		Language:       "en",
		Visibility:     "public",
	}
}
