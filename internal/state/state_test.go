/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package state_test

import (
	"os"
	"testing"

	"codeberg.org/tsmethurst/fedibot/internal/state"
	"codeberg.org/tsmethurst/fedibot/internal/test"
	"github.com/stretchr/testify/suite"
)

type StateTestSuite struct {
	suite.Suite
}

func (suite *StateTestSuite) TestSaveAlreadyPosted() {
	tempDir := suite.T().TempDir()
	tempFile, err := os.CreateTemp(tempDir, "")
	if err != nil {
		suite.FailNow(err.Error())
	}
	tempFile.Close()

	somePosts := test.GetSomePosts()

	err = state.SaveAlreadyPosted(somePosts, tempFile.Name())
	suite.NoError(err)

	b, err := os.ReadFile(tempFile.Name())
	suite.NoError(err)

	suite.Equal(`[{"contentWarning":"this is the first post","text":"this is the text body of the first post","language":"en","visibility":"public"},{"contentWarning":"this is the second post","text":"this is the text body of the second post","language":"en","visibility":"public"},{"contentWarning":"this is the third post","text":"this is the text body of the third post","language":"en","visibility":"public"},{"contentWarning":"this is the fourth post","text":"this is the text body of the fourth post","language":"en","visibility":"public"}]`, string(b))
}

func (suite *StateTestSuite) TestLoadAlreadyPosted() {
	tempDir := suite.T().TempDir()
	tempFile, err := os.CreateTemp(tempDir, "")
	if err != nil {
		suite.FailNow(err.Error())
	}

	_, err = tempFile.Write([]byte(`[{"contentWarning":"this is the first post","text":"this is the text body of the first post","language":"en","visibility":"public"},{"contentWarning":"this is the second post","text":"this is the text body of the second post","language":"en","visibility":"public"},{"contentWarning":"this is the third post","text":"this is the text body of the third post","language":"en","visibility":"public"},{"contentWarning":"this is the fourth post","text":"this is the text body of the fourth post","language":"en","visibility":"public"}]`))
	if err != nil {
		suite.FailNow(err.Error())
	}

	err = tempFile.Close()
	if err != nil {
		suite.FailNow(err.Error())
	}

	alreadyPosted, err := state.LoadAlreadyPosted(tempFile.Name())
	suite.NoError(err)
	suite.NotNil(alreadyPosted)

	suite.EqualValues(test.GetSomePosts(), alreadyPosted)
}

func (suite *StateTestSuite) TestLoadAndSaveAlreadyPostedEmpty() {
	tempDir := suite.T().TempDir()
	tempFile, err := os.CreateTemp(tempDir, "")
	if err != nil {
		suite.FailNow(err.Error())
	}
	tempFile.Close()

	alreadyPosted, err := state.LoadAlreadyPosted(tempFile.Name())
	suite.NoError(err)
	suite.Empty(alreadyPosted)

	alreadyPosted = append(alreadyPosted, test.GetAnotherPost())
	err = state.SaveAlreadyPosted(alreadyPosted, tempFile.Name())
	suite.NoError(err)

	b, err := os.ReadFile(tempFile.Name())
	suite.NoError(err)
	suite.Equal(`[{"contentWarning":"this is the fifth post","text":"this is the text body of the fifth post","language":"en","visibility":"public"}]`, string(b))
}

func (suite *StateTestSuite) TestLoadAndSaveAlreadyPosted() {
	tempDir := suite.T().TempDir()
	tempFile, err := os.CreateTemp(tempDir, "")
	if err != nil {
		suite.FailNow(err.Error())
	}

	_, err = tempFile.Write([]byte(`[{"contentWarning":"this is the first post","text":"this is the text body of the first post","language":"en","visibility":"public"},{"contentWarning":"this is the second post","text":"this is the text body of the second post","language":"en","visibility":"public"},{"contentWarning":"this is the third post","text":"this is the text body of the third post","language":"en","visibility":"public"},{"contentWarning":"this is the fourth post","text":"this is the text body of the fourth post","language":"en","visibility":"public"}]`))
	if err != nil {
		suite.FailNow(err.Error())
	}

	err = tempFile.Close()
	if err != nil {
		suite.FailNow(err.Error())
	}

	alreadyPosted, err := state.LoadAlreadyPosted(tempFile.Name())
	suite.NoError(err)
	suite.NotNil(alreadyPosted)
	suite.EqualValues(test.GetSomePosts(), alreadyPosted)

	alreadyPosted = append(alreadyPosted, test.GetAnotherPost())

	err = state.SaveAlreadyPosted(alreadyPosted, tempFile.Name())
	suite.NoError(err)

	b, err := os.ReadFile(tempFile.Name())
	suite.NoError(err)

	suite.Equal(`[{"contentWarning":"this is the first post","text":"this is the text body of the first post","language":"en","visibility":"public"},{"contentWarning":"this is the second post","text":"this is the text body of the second post","language":"en","visibility":"public"},{"contentWarning":"this is the third post","text":"this is the text body of the third post","language":"en","visibility":"public"},{"contentWarning":"this is the fourth post","text":"this is the text body of the fourth post","language":"en","visibility":"public"},{"contentWarning":"this is the fifth post","text":"this is the text body of the fifth post","language":"en","visibility":"public"}]`, string(b))
}

func TestStateTestSuite(t *testing.T) {
	suite.Run(t, &StateTestSuite{})
}
