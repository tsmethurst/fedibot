/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package state

import (
	"encoding/json"
	"os"

	"codeberg.org/tsmethurst/fedibot/pkg/model"
)

func LoadAlreadyPosted(postedFilePath string) (alreadyPosted []*model.Post, err error) {
	if postedFilePath == "" {
		return
	}

	var b []byte
	b, err = os.ReadFile(postedFilePath)
	if err != nil {
		return
	}
	if len(b) == 0 {
		return
	}

	err = json.Unmarshal(b, &alreadyPosted)
	return
}

func SaveAlreadyPosted(alreadyPosted []*model.Post, postedFilePath string) error {
	if postedFilePath == "" {
		return nil
	}

	b, err := json.Marshal(&alreadyPosted)
	if err != nil {
		return err
	}

	return os.WriteFile(postedFilePath, b, 0666)
}
