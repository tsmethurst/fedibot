/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package postgenerator

import (
	"context"

	"codeberg.org/tsmethurst/fedibot/pkg/model"
)

// ParseFunction describes the actual parsing logic of input -> output
type ParseFunction func(ctx context.Context, input []byte) (output []*model.Post, err error)

// PostGenerator denotes an interface that can do parsing of bytes into posts.
type PostGenerator interface {
	// FromFile parses a slice of posts from the given filepath.
	FromFile(ctx context.Context, filepath string) ([]*model.Post, error)
	RemoveOneFromFile(ctx context.Context, filepath string, post *model.Post) error
}
