/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package postgenerator

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"

	"codeberg.org/tsmethurst/fedibot/pkg/model"
)

var (
	defaultPostRegex *regexp.Regexp = regexp.MustCompile(`quote="(.*)"\nauthor="(.*)"\nlanguage="(.*)"\nsource="(.*)"`)
)

// defaultParseFunction takes input bytes and returns a slice of posts.
// It uses the defaultPostRegex to do this, which expects the input bytes
// to be a list of posts in the following format:
//   quote="Nobody in the world, nobody in history, has ever gotten their freedom by appealing to the moral sense of the people who were oppressing them."
//   author="Assata Olugbala Shakur"
//   language="en"
//   source="https://archive.org/details/Assata/page/n151/mode/1up?q=%22moral+sense%22"
//
//   quote="Nadie en el mundo, nadie en la historia, ha conseguido nunca su libertad apelando al sentido moral de la gente que los oprimía."
//   author="Assata Olugbala Shakur"
//   language="es"
//   source="https://libgen.rs/book/index.php?md5=23C8DA469EB68791DCB71437955DF8D8"
var defaultParseFunction ParseFunction = func(ctx context.Context, input []byte) (output []*model.Post, err error) {
	postMatches := defaultPostRegex.FindAllSubmatch(input, -1)

	for _, postMatch := range postMatches {
		if len(postMatch) != 5 {
			// malformed post, skip it
			continue
		}

		quote := string(postMatch[1])
		author := string(postMatch[2])
		language := string(postMatch[3])
		source := string(postMatch[4])

		quote = strings.ReplaceAll(quote, "\\n", "\n") // unescape newlines

		post := &model.Post{
			ContentWarning: fmt.Sprintf("[%s] %s", language, author),
			Text:           fmt.Sprintf("%s\n\n%s", quote, source),
			Language:       language,
		}

		output = append(output, post)
	}

	return
}

// defaultPostGenerator satisfies the PostGenerator interface using the defaults contained in this package.
type defaultPostGenerator struct {
	parseFunction ParseFunction
}

// NewDefault returns a new default PostGenerator that uses the default parse function.
func NewDefault() PostGenerator {
	return &defaultPostGenerator{
		parseFunction: defaultParseFunction,
	}
}

func (p *defaultPostGenerator) FromFile(ctx context.Context, filepath string) ([]*model.Post, error) {
	b, err := os.ReadFile(filepath)
	if err != nil {
		return nil, err
	}

	return p.parseFunction(ctx, b)
}

func (p *defaultPostGenerator) RemoveOneFromFile(ctx context.Context, filepath string, post *model.Post) error {
	return nil
}
