/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package config

// Values contains contains the type of each configuration value.
type Values struct {
	SourceFilePath string
	PostedFilePath string

	// application and client stuff
	Server       string
	ClientName   string
	Website      string
	ClientID     string
	ClientSecret string
	AuthToken    string
	AccessToken  string
	RedirectURIs string
	Scopes       string

	// status stuff
	StatusContentWarning string
	StatusContent        string
	StatusVisibility     string

	// scheduled post stuff
	PostSchedule string
}
