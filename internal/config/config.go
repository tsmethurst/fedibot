/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package config

import (
	"strings"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func InitViper(f *pflag.FlagSet) error {
	// environment variable stuff
	// flag 'some-flag-name' becomes env var 'FEDIBOT_SOME_FLAG_NAME'
	viper.SetEnvPrefix("fedibot")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	// flag stuff
	// bind all of the flags in flagset to viper so that we can retrieve their values from the viper store
	if err := viper.BindPFlags(f); err != nil {
		return err
	}

	return nil
}
