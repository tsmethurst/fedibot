/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package config

// Defaults returns a populated Values struct with most of the values set to reasonable defaults.
// Note that if you use this, you still need to set Host and, if desired, ConfigPath.
var Defaults = Values{
	SourceFilePath: "",
	PostedFilePath: "",

	// application and client stuff
	Server:       "",
	ClientName:   "Fedibot",
	Website:      "https://codeberg.org/tsmethurst/fedibot",
	ClientID:     "",
	ClientSecret: "",
	AuthToken:    "",
	AccessToken:  "",
	RedirectURIs: "urn:ietf:wg:oauth:2.0:oob",
	Scopes:       "read write",

	// status stuff
	StatusContentWarning: "",
	StatusContent:        "",
	StatusVisibility:     "unlisted",

	// scheduled post stuff
	PostSchedule: "0 12 * * *", // noon every day
}
