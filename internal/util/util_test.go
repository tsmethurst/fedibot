/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package util_test

import (
	"testing"

	"codeberg.org/tsmethurst/fedibot/internal/test"
	"codeberg.org/tsmethurst/fedibot/internal/util"
	"codeberg.org/tsmethurst/fedibot/pkg/model"
	"github.com/stretchr/testify/suite"
)

type UtilTestSuite struct {
	suite.Suite
}

func (suite *UtilTestSuite) TestSelectRandom() {
	alreadyPosted := test.GetSomePosts()

	possiblePosts := test.GetSomePosts()
	possiblePosts = append(possiblePosts, test.GetAnotherPost()) // leave only one option so we know it'll post this one

	post, err := util.RandomUnposted(alreadyPosted, possiblePosts)
	suite.NoError(err)
	suite.NotNil(post)

	suite.EqualValues(test.GetAnotherPost(), post)
}

func (suite *UtilTestSuite) TestSelectRandomNoneLeft() {
	alreadyPosted := test.GetSomePosts()
	possiblePosts := test.GetSomePosts()

	post, err := util.RandomUnposted(alreadyPosted, possiblePosts)
	suite.EqualError(err, "no posts remaining in possiblePosts that haven't been posted already")
	suite.Nil(post)
}

func (suite *UtilTestSuite) TestSelectRandomNonePostedYet() {
	alreadyPosted := []*model.Post{}
	possiblePosts := test.GetSomePosts()
	post, err := util.RandomUnposted(alreadyPosted, possiblePosts)
	suite.NoError(err)
	suite.NotNil(post)
}

func TestUtilTestSuite(t *testing.T) {
	suite.Run(t, &UtilTestSuite{})
}
