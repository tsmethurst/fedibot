/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package util

import (
	"crypto/rand"
	"errors"
	"math/big"

	"codeberg.org/tsmethurst/fedibot/pkg/model"
)

func RandomUnposted(alreadyPosted []*model.Post, possiblePosts []*model.Post) (*model.Post, error) {
	shortlist := []*model.Post{}

	for _, possiblePost := range possiblePosts {
		isCandidate := true

		if len(alreadyPosted) != 0 {
		candidateLoop:
			for _, p := range alreadyPosted {
				// not a candidate anymore if it's identical to one we've already posted
				if possiblePost.ContentWarning == p.ContentWarning && possiblePost.Text == p.Text && possiblePost.Language == p.Language && possiblePost.Visibility == p.Visibility {
					isCandidate = false
					break candidateLoop
				}
			}
		}

		if isCandidate {
			shortlist = append(shortlist, possiblePost)
		}
	}

	shortlistLength := len(shortlist)
	if shortlistLength == 0 {
		return nil, errors.New("no posts remaining in possiblePosts that haven't been posted already")
	}

	randomIndex, err := rand.Int(rand.Reader, big.NewInt(int64(shortlistLength)))
	if err != nil {
		return nil, err
	}

	return shortlist[randomIndex.Int64()], nil
}
