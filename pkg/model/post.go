/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package model

// Post represents a simple post to a Mastodon API.
type Post struct {
	ContentWarning string `json:"contentWarning"`
	Text           string `json:"text"`
	Language       string `json:"language"`
	Visibility     string `json:"visibility"`
}
