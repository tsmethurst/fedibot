/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package bot

import (
	"errors"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"github.com/mattn/go-mastodon"
	"github.com/spf13/viper"
)

type Bot struct {
	mastodonConfig *mastodon.Config
}

func New() (*Bot, error) {
	server := viper.GetString(config.Keys.Server)
	if server == "" {
		return nil, errors.New("server was not set")
	}

	mastodonConfig := &mastodon.Config{
		Server:       server,
		ClientID:     viper.GetString(config.Keys.ClientID),
		ClientSecret: viper.GetString(config.Keys.ClientSecret),
		AccessToken:  viper.GetString(config.Keys.AccessToken),
	}

	return &Bot{
		mastodonConfig: mastodonConfig,
	}, nil
}
