/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package bot

import (
	"context"

	"codeberg.org/tsmethurst/fedibot/pkg/model"
	"github.com/mattn/go-mastodon"
)

func (b *Bot) PostStatus(ctx context.Context, post *model.Post) (*mastodon.Status, error) {
	client := mastodon.NewClient(b.mastodonConfig)

	return client.PostStatus(ctx, &mastodon.Toot{
		SpoilerText: post.ContentWarning,
		Status:      post.Text,
		Visibility:  post.Visibility,
	})
}
