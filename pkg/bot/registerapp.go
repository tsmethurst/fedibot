/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package bot

import (
	"context"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"github.com/mattn/go-mastodon"
	"github.com/spf13/viper"
)

func (b *Bot) RegisterApp(ctx context.Context) (*mastodon.Application, error) {
	appConfig := &mastodon.AppConfig{
		Server:       b.mastodonConfig.Server,
		ClientName:   viper.GetString(config.Keys.ClientName),
		RedirectURIs: viper.GetString(config.Keys.RedirectURIs),
		Scopes:       viper.GetString(config.Keys.Scopes),
		Website:      viper.GetString(config.Keys.Website),
	}

	return mastodon.RegisterApp(ctx, appConfig)
}
