/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"os"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"codeberg.org/tsmethurst/fedibot/internal/encoder"
	"codeberg.org/tsmethurst/fedibot/pkg/bot"
	"github.com/spf13/cobra"
)

func registerAppCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "register-app",
		Short: "register an application with the given instance",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(cmd.Context(), func(ctx context.Context) error {
				bot, err := bot.New()
				if err != nil {
					return err
				}

				app, err := bot.RegisterApp(ctx)
				if err != nil {
					return err
				}

				return encoder.New(os.Stdout).Encode(app)
			})
		},
	}

	cmd.Flags().String(config.Keys.Server, config.Defaults.Server, usage.Server)
	cmd.Flags().String(config.Keys.ClientName, config.Defaults.ClientName, usage.ClientName)
	cmd.Flags().String(config.Keys.Website, config.Defaults.Website, usage.Website)
	cmd.Flags().String(config.Keys.RedirectURIs, config.Defaults.RedirectURIs, usage.RedirectURIs)
	cmd.Flags().String(config.Keys.Scopes, config.Defaults.Scopes, usage.Scopes)

	return cmd
}
