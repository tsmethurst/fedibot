/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import "codeberg.org/tsmethurst/fedibot/internal/config"

var usage = config.KeyNames{
	SourceFilePath: "source file path from which to derive a corpus",
	PostedFilePath: "file to store data about already-posted statuses in, to avoid repetition; will still work OK if this is not set, or is set to an empty string, but posts will repeat sometimes",

	// application and client stuff
	Server:       "server to connect to, eg https://mastodon.social",
	ClientName:   "client name for application registration",
	Website:      "website for application registration",
	ClientID:     "oauth client id",
	ClientSecret: "oauth client secret",
	AuthToken:    "oauth auth token",
	AccessToken:  "oauth access token",
	RedirectURIs: "application/client redirect URIs",
	Scopes:       "list of scopes to register with/use",

	// scheduled post stuff
	PostSchedule: "cron string for the schedule",
}
