/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"os"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"codeberg.org/tsmethurst/fedibot/internal/encoder"
	"codeberg.org/tsmethurst/fedibot/pkg/bot"
	"github.com/spf13/cobra"
)

func authenticateTokenCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "token-authenticate",
		Short: "authenticate an auth token to get an access token",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(cmd.Context(), func(ctx context.Context) error {
				bot, err := bot.New()
				if err != nil {
					return err
				}

				accessToken, err := bot.AuthenticateToken(ctx)
				if err != nil {
					return err
				}

				return encoder.New(os.Stdout).Encode(accessToken)
			})
		},
	}

	cmd.Flags().String(config.Keys.Server, config.Defaults.Server, usage.Server)
	cmd.Flags().String(config.Keys.ClientID, config.Defaults.ClientID, usage.ClientID)
	cmd.Flags().String(config.Keys.ClientSecret, config.Defaults.ClientSecret, usage.ClientSecret)
	cmd.Flags().String(config.Keys.AuthToken, config.Defaults.AuthToken, usage.AuthToken)
	cmd.Flags().String(config.Keys.RedirectURIs, config.Defaults.RedirectURIs, usage.RedirectURIs)

	return cmd
}
