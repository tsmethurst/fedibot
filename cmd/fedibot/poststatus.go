/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"os"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"codeberg.org/tsmethurst/fedibot/internal/encoder"
	"codeberg.org/tsmethurst/fedibot/pkg/bot"
	"codeberg.org/tsmethurst/fedibot/pkg/model"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func postStatusCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "status-post",
		Short: "post a status to the given instance",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(cmd.Context(), func(ctx context.Context) error {
				bot, err := bot.New()
				if err != nil {
					return err
				}

				status, err := bot.PostStatus(ctx, &model.Post{
					ContentWarning: viper.GetString(config.Keys.StatusContentWarning),
					Text:           viper.GetString(config.Keys.StatusContent),
					Visibility:     viper.GetString(config.Keys.StatusVisibility),
				})
				if err != nil {
					return err
				}

				return encoder.New(os.Stdout).Encode(status)
			})
		},
	}

	cmd.Flags().String(config.Keys.Server, config.Defaults.Server, usage.Server)

	cmd.Flags().String(config.Keys.AccessToken, config.Defaults.AccessToken, usage.AccessToken)

	cmd.Flags().String(config.Keys.StatusContentWarning, config.Defaults.StatusContentWarning, usage.StatusContentWarning)
	cmd.Flags().String(config.Keys.StatusContent, config.Defaults.StatusContent, usage.StatusContent)
	cmd.Flags().String(config.Keys.StatusVisibility, config.Defaults.StatusVisibility, usage.StatusVisibility)

	return cmd
}
