/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"fmt"
	"runtime/debug"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var Version string

func main() {
	buildInfo, ok := debug.ReadBuildInfo()
	if !ok {
		panic("could not read buildinfo")
	}

	goVersion := buildInfo.GoVersion
	var commit string
	var time string
	for _, s := range buildInfo.Settings {
		if s.Key == "vcs.revision" {
			commit = s.Value[:7]
		}
		if s.Key == "vcs.time" {
			time = s.Value
		}
	}

	var versionString string
	if Version != "" {
		versionString = fmt.Sprintf("%s %s %s [%s]", Version, commit, time, goVersion)
	}

	rootCmd := &cobra.Command{
		Use:           "fedibot",
		Short:         "fedibot - a bot cli tool for posting to mastodon apis",
		Long:          "fedibot - a bot cli tool for posting to mastodon apis\nhttps://codeberg.org/tsmethurst/fedibot",
		Version:       versionString,
		SilenceErrors: true,
		SilenceUsage:  true,
	}

	rootCmd.AddCommand(parseCommand())
	rootCmd.AddCommand(registerAppCommand())
	rootCmd.AddCommand(postStatusCommand())
	rootCmd.AddCommand(postRandomFromCorpusCommand())
	rootCmd.AddCommand(authenticateTokenCommand())
	rootCmd.AddCommand(scheduleCommand())

	if err := rootCmd.Execute(); err != nil {
		logrus.Fatalf("error executing command: %s", err)
	}
}

// preRun should be run in the pre-run stage of every cobra command.
// The goal here is to initialize the viper config store
func preRun(cmd *cobra.Command) error {
	if err := config.InitViper(cmd.Flags()); err != nil {
		return fmt.Errorf("error initializing viper: %s", err)
	}
	return nil
}

// run should be used during the run stage of every cobra command.
// The idea here is to take an action and run it with the given
// context, after initializing any last-minute things like loggers, store, etc.
func run(ctx context.Context, action func(innerCtx context.Context) error) error {
	return action(ctx)
}
