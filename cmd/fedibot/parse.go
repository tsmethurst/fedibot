/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"os"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"codeberg.org/tsmethurst/fedibot/internal/encoder"
	"codeberg.org/tsmethurst/fedibot/internal/postgenerator"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func parseCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "parse",
		Short: "try parsing a post source, and print the parsed posts as json",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(cmd.Context(), func(ctx context.Context) error {
				p := postgenerator.NewDefault()

				posts, err := p.FromFile(ctx, viper.GetString(config.Keys.SourceFilePath))
				if err != nil {
					return err
				}

				return encoder.New(os.Stdout).Encode(posts)
			})
		},
	}

	cmd.Flags().String(config.Keys.SourceFilePath, config.Defaults.SourceFilePath, usage.SourceFilePath)

	return cmd
}
