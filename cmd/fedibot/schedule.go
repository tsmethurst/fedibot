/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"encoding/json"
	"errors"
	"os"
	"os/signal"
	"syscall"
	"time"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"codeberg.org/tsmethurst/fedibot/internal/postgenerator"
	"codeberg.org/tsmethurst/fedibot/internal/state"
	"codeberg.org/tsmethurst/fedibot/internal/util"
	"codeberg.org/tsmethurst/fedibot/pkg/bot"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func scheduleCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "schedule",
		Short: "post statuses at random from the corpus according to the given cron schedule",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(cmd.Context(), postScheduled)
		},
	}

	cmd.Flags().String(config.Keys.SourceFilePath, config.Defaults.SourceFilePath, usage.SourceFilePath)
	cmd.Flags().String(config.Keys.PostedFilePath, config.Defaults.PostedFilePath, usage.PostedFilePath)
	cmd.Flags().String(config.Keys.Server, config.Defaults.Server, usage.Server)
	cmd.Flags().String(config.Keys.AccessToken, config.Defaults.AccessToken, usage.AccessToken)
	cmd.Flags().String(config.Keys.PostSchedule, config.Defaults.PostSchedule, usage.PostSchedule)
	return cmd
}

func postScheduled(ctx context.Context) error {
	bot, err := bot.New()
	if err != nil {
		logrus.Error(err)
	}

	acct, err := bot.VerifyCredentials(ctx)
	if err != nil {
		return err
	}
	logrus.Infof("connected with account %s username %s", acct.ID, acct.Username)

	c := cron.New()
	if eID, err := c.AddFunc(viper.GetString(config.Keys.PostSchedule), postScheduledJob(ctx, bot)); err != nil {
		return err
	} else {
		c.Start()
		logrus.Infof("first post scheduled for %s", c.Entry(eID).Next)
	}

	// wait to catch shutdown signals
	// from the operating system
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGTERM)
	sig := <-sigs
	logrus.Infof("received signal %s, shutting down", sig)

	stopping := c.Stop()
	select {
	case <-stopping.Done():
		break
	case <-time.After(30 * time.Second):
		logrus.Warn("stopping cron forcefully")
	}

	logrus.Info("done! exiting...")
	return nil
}

func postScheduledJob(ctx context.Context, bot *bot.Bot) func() {
	return func() {
		postedFilePath := viper.GetString(config.Keys.PostedFilePath)

		alreadyPosted, err := state.LoadAlreadyPosted(postedFilePath)
		if err != nil {
			logrus.Error(err)
			return
		}

		possiblePosts, err := postgenerator.NewDefault().FromFile(ctx, viper.GetString(config.Keys.SourceFilePath))
		if err != nil {
			logrus.Error(err)
			return
		}

		if len(possiblePosts) == 0 {
			logrus.Error(errors.New("length of corpus was 0, cannot post from it"))
			return
		}

		post, err := util.RandomUnposted(alreadyPosted, possiblePosts)
		if err != nil {
			logrus.Error(err)
			return
		}

		status, err := bot.PostStatus(ctx, post)
		if err != nil {
			logrus.Error(err)
			return
		}

		b, err := json.Marshal(status)
		if err != nil {
			logrus.Error(err)
			return
		}

		logrus.Infof("posted status\n\n%s", string(b))

		alreadyPosted = append(alreadyPosted, post)
		if err := state.SaveAlreadyPosted(alreadyPosted, postedFilePath); err != nil {
			logrus.Error(err)
		}
	}
}
