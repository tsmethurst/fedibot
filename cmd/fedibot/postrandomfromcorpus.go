/*
   Copyright (C) 2022 Tobi Smethurst <tobi.smethurst@protonmail.com>

   This file is part of Fedibot.

   This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
   applicable law. It may and probably will break. You are free to copy and reuse and modify
   this code, but if you use it for anything other than SMASHING THE STATE, you will be
   hunted down by angry bats.
*/

package main

import (
	"context"
	"crypto/rand"
	"errors"
	"math/big"
	"os"

	"codeberg.org/tsmethurst/fedibot/internal/config"
	"codeberg.org/tsmethurst/fedibot/internal/encoder"
	"codeberg.org/tsmethurst/fedibot/internal/postgenerator"
	"codeberg.org/tsmethurst/fedibot/pkg/bot"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func postRandomFromCorpusCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "status-post-random-from-corpus",
		Short: "post a status to the given instance, selecting at random from the given corpus",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(cmd.Context(), func(ctx context.Context) error {
				p := postgenerator.NewDefault()

				posts, err := p.FromFile(ctx, viper.GetString(config.Keys.SourceFilePath))
				if err != nil {
					return err
				}

				if len(posts) == 0 {
					return errors.New("length of corpus was 0, cannot post from it")
				}

				// select a random post
				randomIndex, err := rand.Int(rand.Reader, big.NewInt(int64(len(posts))))
				if err != nil {
					return err
				}
				post := posts[randomIndex.Int64()]

				bot, err := bot.New()
				if err != nil {
					return err
				}

				status, err := bot.PostStatus(ctx, post)
				if err != nil {
					return err
				}

				return encoder.New(os.Stdout).Encode(status)
			})
		},
	}

	cmd.Flags().String(config.Keys.SourceFilePath, config.Defaults.SourceFilePath, usage.SourceFilePath)

	cmd.Flags().String(config.Keys.Server, config.Defaults.Server, usage.Server)

	cmd.Flags().String(config.Keys.AccessToken, config.Defaults.AccessToken, usage.AccessToken)

	cmd.Flags().String(config.Keys.StatusContentWarning, config.Defaults.StatusContentWarning, usage.StatusContentWarning)
	cmd.Flags().String(config.Keys.StatusContent, config.Defaults.StatusContent, usage.StatusContent)
	cmd.Flags().String(config.Keys.StatusVisibility, config.Defaults.StatusVisibility, usage.StatusVisibility)

	return cmd
}
